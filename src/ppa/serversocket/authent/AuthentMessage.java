package ppa.serversocket.authent;

public enum AuthentMessage {
	LOGIN("enter your login"),
	PASSWORD("enter your password"),
	CONNECTED("user connected");
	
	private String message;
	
	AuthentMessage(String msg) {
		this.message = msg;
	}
	
	public String getMessage () {
		return this.message;
	}
}
