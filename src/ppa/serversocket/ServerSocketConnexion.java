package ppa.serversocket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import ppa.serversocket.authent.OauthResponseHandler;

public class ServerSocketConnexion implements Runnable {
	private ServerSocket serverSocket;
	private Socket socket;
	
	public Thread authentThread;
	
	public ServerSocketConnexion (ServerSocket serverSocket) {
		this.serverSocket	= serverSocket;
	}
	@Override
	public void run() {
		try {
			socket	= serverSocket.accept();
			System.out.println("connexion accepted by server : "  + socket.getLocalSocketAddress() + " : " + socket.getLocalPort());
			OauthResponseHandler oauthHandler = new OauthResponseHandler(socket);
			oauthHandler.run();
		}
		catch ( IOException e ) {
			System.err.println("server error !!\n" + e.getMessage());
		}		
	}
}