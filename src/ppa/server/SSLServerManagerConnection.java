package ppa.server;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import ppa.http.utils.Logger;

public class SSLServerManagerConnection {
	private static Logger<SSLServerManagerConnection> LOG			= Logger.getLogger(SSLServerManagerConnection.class);
	private SSLServerSocket sslSocketServer;
	private SSLContext sslContext;
	private String keystoreType;
	private boolean isServerDone	= false;
	private int port;
	private String fileKeystore;
	private String password;
	private String algo;
	private String tlsVersion;
	
	public SSLServerManagerConnection (int _port, String _keystoreType, String _fileKeystore, String _password, String _algo, String _tlsVersion) {
		port			= _port;
		keystoreType	= _keystoreType;
		fileKeystore	= _fileKeystore;
		password		= _password;
		algo			= _algo;
		tlsVersion		= _tlsVersion;
	}
	
	public void initServer () throws IOException, UnrecoverableKeyException, KeyManagementException, KeyStoreException, NoSuchAlgorithmException, CertificateException {
		sslContext		= this.createSSLContext(fileKeystore, password, algo, tlsVersion);
		
		// create server socket
		sslSocketServer = (SSLServerSocket) sslContext.getServerSocketFactory().createServerSocket(port);
		
		LOG.debug("[socketServer] " + "getLocalPort" + " : " + sslSocketServer.getLocalPort());
		LOG.debug("[socketServer] " + "getInetAddress" + " : " + sslSocketServer.getInetAddress());
		LOG.debug("[socketServer InetAddress] " + "getHostAddress" + " : " + sslSocketServer.getInetAddress().getHostAddress());
		LOG.debug("[socketServer InetAddress] " + "getHostName" + " : " + sslSocketServer.getInetAddress().getHostName());
		LOG.debug("[socketServer InetAddress] " + "getCanonicalHostName" + " : " + sslSocketServer.getInetAddress().getCanonicalHostName());
		LOG.debug("[socketServer InetAddress] " + "hashCode" + " : " + sslSocketServer.getInetAddress().hashCode());
		LOG.debug("[socketServer] " + "getLocalSocketAddress" + " : " + sslSocketServer.getLocalSocketAddress());
		LOG.debug("[socketServer] " + "getReuseAddress" + " : " + sslSocketServer.getReuseAddress());
	}
	
	public SSLContext createSSLContext (String fileKeystore, String password, String algo, String _TLSversion) throws 
	KeyStoreException, 
	NoSuchAlgorithmException, 
	CertificateException, 
	FileNotFoundException, 
	IOException, 
	UnrecoverableKeyException, KeyManagementException {
		SSLContext _sslContext	= null;
		KeyStore keystore		= KeyStore.getInstance(keystoreType);
		keystore.load(new FileInputStream(fileKeystore), password.toCharArray());
		
		LOG.debug("[keystore] " + "containsAlias" + " : " + keystore.containsAlias("simulator"));
		
		// create keyManager
		KeyManagerFactory keyManagerFactory	= KeyManagerFactory.getInstance(algo);
		keyManagerFactory.init(keystore, password.toCharArray());
		KeyManager[] keyManager	= keyManagerFactory.getKeyManagers();
		
		// create trust manager
		TrustManagerFactory trustManagerFactory	= TrustManagerFactory.getInstance(algo);
		trustManagerFactory.init(keystore);
		TrustManager[] trustManager	= trustManagerFactory.getTrustManagers();
		
		// initialize sslcontext
		_sslContext	= SSLContext.getInstance(_TLSversion);		
		_sslContext.init(keyManager, trustManager, null);
		
		return _sslContext;
	}
	
	public void listenConnection () throws IOException {
		while ( !isServerDone ) {			
			SSLClientManager sslClientManager	= new SSLClientManager((SSLSocket) sslSocketServer.accept());
			LOG.debug("[clientManager] " + "clientManager" + " : " + sslClientManager.toString());
			LOG.debug("[ClientSocket] " + "getPort" + " : " + sslClientManager.getSslClientSocket().getPort());
			LOG.debug("[ClientSocket] " + "getLocalPort" + " : " + sslClientManager.getSslClientSocket().getLocalPort());
			LOG.debug("[ClientSocket InetAddress] " + "getHostAddress" + " : " + sslClientManager.getSslClientSocket().getInetAddress().getHostAddress());
			LOG.debug("[ClientSocket InetAddress] " + "getHostName" + " : " + sslClientManager.getSslClientSocket().getInetAddress().getHostName());
			LOG.debug("[ClientSocket InetAddress] " + "getCanonicalHostName" + " : " + sslClientManager.getSslClientSocket().getInetAddress().getCanonicalHostName());
			LOG.debug("[ClientSocket InetAddress] " + "hashCode" + " : " + sslClientManager.getSslClientSocket().getInetAddress().hashCode());
			Thread thread	= new Thread(sslClientManager);
			thread.start();
		}
	}

	public SSLServerSocket getSslSocketServer() {
		return sslSocketServer;
	}

	public void setSslSocketServer(SSLServerSocket sslSocketServer) {
		this.sslSocketServer = sslSocketServer;
	}

	public SSLContext getSslContext() {
		return sslContext;
	}

	public void setSslContext(SSLContext sslContext) {
		this.sslContext = sslContext;
	}

	public String getKeystoreType() {
		return keystoreType;
	}

	public void setKeystoreType(String keystoreType) {
		this.keystoreType = keystoreType;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}