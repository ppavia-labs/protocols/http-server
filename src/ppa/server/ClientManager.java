package ppa.server;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import ppa.http.utils.HttpUtils;
import ppa.http.utils.Logger;
import ppa.httpstream.model.HttpStream;

public class ClientManager implements Runnable {
	private static Logger<ClientManager> log			= Logger.getLogger(ClientManager.class);
	private BufferedReader in;
	private PrintWriter out;
	public Thread thread;
	private BufferedInputStream bis;
	private Socket clientSocket;
	
	public ClientManager (Socket _clientSocket) {
		clientSocket = _clientSocket;
	}

	@Override
	public void run() {
		try {
			bis	= new BufferedInputStream(clientSocket.getInputStream());
			in	= new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())),true);
			if ( in == null ) {
				System.err.println("OauthResponseHandler : BufferedReader in is null !!");
			}
			if ( out == null ) {
				System.err.println("OauthResponseHandler : PrintWriter out is null !!");
			}
			log.info("clientSocket : " + clientSocket.getReceiveBufferSize());
			log.info("clientSocket : " + clientSocket.getKeepAlive());
	
			HttpStream httpStream	= HttpUtils.readStream(in);
			
			log.info("httpStream : \n" + httpStream.toString());
			
			if ( httpStream != null ) {
				String response	= mockJsonContentResponse();				
				out.print(response);
				out.println();
				out.flush();
				log.info("response : \n" + response);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if ( in != null ) {
				try {
					in.close();
					log.info("in : close");
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
			if ( out != null ) {
				try {
					out.close();
					log.info("out : close");
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public String mockJsonContentResponse () {
		// content
		StringBuilder content	= new StringBuilder("");
		content.append("{");
		content.append("\"access_token\"").append(":").append("{");
		content.append("\"uid\"").append(":").append("\"H66319\"").append(",");
		content.append("\"mail\"").append(":").append("\"ppavia.externe@macif.fr\"").append(",");
		content.append("\"cdCentre\"").append(":").append("\"27\"").append(",");
		content.append("\"roles\"").append(":").append("\"\"").append(",");
		content.append("\"pi.sri\"").append(":").append("\"eMleueSn_33qBC3gFq_fyfuJx40..zXlJ\"").append(",");
		content.append("\"region\"").append(":").append("\"SIEGE\"").append(",");
		content.append("\"nom\"").append(":").append("\"PAVIA\"").append(",");
		content.append("\"prenom\"").append(":").append("\"Pierre\"").append(",");
		content.append("\"fonctionRH\"").append(":").append("\"Autre Fonction\"").append("");
		content.append("}").append(",");
		content.append("\"scope\"").append(":").append("\"openid\"").append(",");
		content.append("\"token_type\"").append(":").append("\"urn:\"").append(",");
		content.append("\"expires_in\"").append(":").append(7159).append(",");
		content.append("\"client_id\"").append(":").append("\"facette\"").append("");
		content.append("}");
		
		byte[] postData			= content.toString().getBytes( StandardCharsets.UTF_8 );
		int    postDataLength	= postData.length;
		
		StringBuilder response	= new StringBuilder("HTTP/1.0 200 OK").append(HttpUtils.CRLF);
		response.append("Content-Length").append(":").append(postDataLength).append(HttpUtils.CRLF);
		response.append("Content-Type").append(":").append("application/json").append(HttpUtils.CRLF);
		response.append("HTTP-date").append(":").append(new Date()).append(HttpUtils.CRLF);
		response.append(HttpUtils.CRLF);
		response.append(content.toString());
		response.append(HttpUtils.CRLF);
		
		return response.toString();
	}

	public BufferedReader getIn() {
		return in;
	}

	public void setIn(BufferedReader in) {
		this.in = in;
	}

	public PrintWriter getOut() {
		return out;
	}

	public void setOut(PrintWriter out) {
		this.out = out;
	}

	public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}

	public BufferedInputStream getBis() {
		return bis;
	}

	public void setBis(BufferedInputStream bis) {
		this.bis = bis;
	}

	public Socket getClientSocket() {
		return clientSocket;
	}

	public void setClientSocket(Socket clientSocket) {
		this.clientSocket = clientSocket;
	}
}
