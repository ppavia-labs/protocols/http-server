package ppa.server;

import java.io.IOException;
import java.net.ServerSocket;

import ppa.http.utils.Logger;

public class ServerManagerConnection {
	private static Logger<ServerManagerConnection> LOG			= Logger.getLogger(ServerManagerConnection.class);
	private ServerSocket socketServer;
	private int port;
	
	public ServerManagerConnection (int _port) {
		port	= _port;
	}
	
	public void initServer () throws IOException {
		socketServer = new ServerSocket(port);
		LOG.debug("[socketServer] " + "getLocalPort" + " : " + socketServer.getLocalPort());
		LOG.debug("[socketServer] " + "getInetAddress" + " : " + socketServer.getInetAddress());
		LOG.debug("[socketServer InetAddress] " + "getHostAddress" + " : " + socketServer.getInetAddress().getHostAddress());
		LOG.debug("[socketServer InetAddress] " + "getHostName" + " : " + socketServer.getInetAddress().getHostName());
		LOG.debug("[socketServer InetAddress] " + "getCanonicalHostName" + " : " + socketServer.getInetAddress().getCanonicalHostName());
		LOG.debug("[socketServer InetAddress] " + "hashCode" + " : " + socketServer.getInetAddress().hashCode());
		LOG.debug("[socketServer] " + "getLocalSocketAddress" + " : " + socketServer.getLocalSocketAddress());
		LOG.debug("[socketServer] " + "getReuseAddress" + " : " + socketServer.getReuseAddress());
	}
	
	public void listenConnection () throws IOException {
		while ( true ) {			
			ClientManager clientManager	= new ClientManager(socketServer.accept());
			LOG.debug("[clientManager] " + "clientManager" + " : " + clientManager.toString());
			LOG.debug("[ClientSocket] " + "getPort" + " : " + clientManager.getClientSocket().getPort());
			LOG.debug("[ClientSocket] " + "getLocalPort" + " : " + clientManager.getClientSocket().getLocalPort());
			LOG.debug("[ClientSocket InetAddress] " + "getHostAddress" + " : " + clientManager.getClientSocket().getInetAddress().getHostAddress());
			LOG.debug("[ClientSocket InetAddress] " + "getHostName" + " : " + clientManager.getClientSocket().getInetAddress().getHostName());
			LOG.debug("[ClientSocket InetAddress] " + "getCanonicalHostName" + " : " + clientManager.getClientSocket().getInetAddress().getCanonicalHostName());
			LOG.debug("[ClientSocket InetAddress] " + "hashCode" + " : " + clientManager.getClientSocket().getInetAddress().hashCode());
			Thread thread	= new Thread(clientManager);
			thread.start();
		}
	}

	public ServerSocket getSocketServer() {
		return socketServer;
	}

	public void setSocketServer(ServerSocket socketServer) {
		this.socketServer = socketServer;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
}