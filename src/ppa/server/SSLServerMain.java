package ppa.server;

import java.io.File;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import ppa.http.utils.Logger;

public class SSLServerMain {
	private static Logger<SSLServerMain> LOG			= Logger.getLogger(SSLServerMain.class);
	private static String fileKeystore					= File.separator + "etc" + File.separator + "ssl" + File.separator + "certs" + File.separator + "apps.keystore";
	
	public static void main (String...args) {
		int port				= 2009;
		String keystoreType		= "JKS";
		String password			= "mamacif";
		String algo				= "SunX509";
		String tlsVersion		= "TLSv1";

		LOG.info("[main] " + "fileKeystore" + " : " + fileKeystore);
		
		SSLServerManagerConnection SSLServerManager	= new SSLServerManagerConnection(port, keystoreType, fileKeystore, password, algo, tlsVersion);
		
		try {
			SSLServerManager.initServer();
			SSLServerManager.listenConnection();
		} catch (IOException e) {
			LOG.error("[initServer] " + "port" + " : " + SSLServerManager.getPort());
			LOG.error("[IOException] " + e.getMessage());
		} catch (UnrecoverableKeyException e) {
			LOG.error("[initServer] " + "port" + " : " + SSLServerManager.getPort());
			LOG.error("[UnrecoverableKeyException] " + e.getMessage());
		} catch (KeyManagementException e) {
			LOG.error("[initServer] " + "port" + " : " + SSLServerManager.getPort());
			LOG.error("[KeyManagementException] " + e.getMessage());
		} catch (KeyStoreException e) {
			LOG.error("[initServer] " + "port" + " : " + SSLServerManager.getPort());
			LOG.error("[KeyStoreException] " + e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			LOG.error("[initServer] " + "port" + " : " + SSLServerManager.getPort());
			LOG.error("[NoSuchAlgorithmException] " + e.getMessage());
		} catch (CertificateException e) {
			LOG.error("[initServer] " + "port" + " : " + SSLServerManager.getPort());
			LOG.error("[CertificateException] " + e.getMessage());
		}
	}
}
