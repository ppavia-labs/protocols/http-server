package ppa.server;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.Date;

import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;

import ppa.http.utils.HttpUtils;
import ppa.http.utils.Logger;
import ppa.httpstream.model.HttpStream;

public class SSLClientManager implements Runnable {
	private static Logger<SSLClientManager> log			= Logger.getLogger(SSLClientManager.class);
	private BufferedReader br;
	private PrintWriter pw;
	public Thread thread;
	private BufferedInputStream is;
	private SSLSocket sslClientSocket;
	
	public SSLClientManager (SSLSocket _clientSocket) {
		sslClientSocket = _clientSocket;
	}

	@Override
	public void run() {
		sslClientSocket.setEnabledCipherSuites(sslClientSocket.getSupportedCipherSuites());
		try {
			// start handshake
			sslClientSocket.startHandshake();
			
			// get session after the connection is established
			SSLSession sslSession	= sslClientSocket.getSession();
			
			log.debug("[sslSession] " + "getProtocol" + " : " + sslSession.getProtocol());
			log.debug("[sslSession] " + "getCipherSuite" + " : " + sslSession.getCipherSuite());
			
			is	= new BufferedInputStream(sslClientSocket.getInputStream());
			br	= new BufferedReader(new InputStreamReader(sslClientSocket.getInputStream()));
			pw	= new PrintWriter(new BufferedWriter(new OutputStreamWriter(sslClientSocket.getOutputStream())),true);
			
			if ( br == null ) {
				System.err.println("OauthResponseHandler : BufferedReader in is null !!");
			}
			if ( pw == null ) {
				System.err.println("OauthResponseHandler : PrintWriter out is null !!");
			}
			
			log.debug("[sslClientSocket] " + "getReceiveBufferSize" + " : " + sslClientSocket.getReceiveBufferSize());
			log.debug("[sslClientSocket] " + "getKeepAlive" + " : " + sslClientSocket.getKeepAlive());
	
			HttpStream httpStream	= HttpUtils.readSSLStream(is);
			
			log.info("httpStream : \n" + httpStream.toString());
			
			if ( httpStream != null ) {
				String response	= mockJsonContentResponse();				
				pw.print(response);
				pw.println();
				pw.flush();
				log.debug("[mockJsonContentResponse] " + "response" + " : " + response);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			if ( sslClientSocket != null ) {
				try {
					sslClientSocket.close();
					log.debug("[finally] " + "sslClientSocket" + " : " + "close");
				}
				catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public String mockJsonContentResponse () {
		// content
		StringBuilder content	= new StringBuilder("");
		content.append("{");
		content.append("\"access_token\"").append(":").append("{");
		content.append("\"uid\"").append(":").append("\"S41462\"").append(",");
		content.append("\"mail\"").append(":").append("\"timothee.dalbadie@macif.fr\"").append(",");
		content.append("\"cdCentre\"").append(":").append("\"27\"").append(",");
		content.append("\"roles\"").append(":").append("\"\"").append(",");
		content.append("\"pi.sri\"").append(":").append("\"eMleueSn_33qBC3gFq_fyfuJx40..zXlJ\"").append(",");
		content.append("\"region\"").append(":").append("\"SIEGE\"").append(",");
		content.append("\"nom\"").append(":").append("\"DALBADIE\"").append(",");
		content.append("\"prenom\"").append(":").append("\"Timothee\"").append(",");
		content.append("\"fonctionRH\"").append(":").append("\"Autre Fonction\"").append("");
		content.append("}").append(",");
		content.append("\"scope\"").append(":").append("\"openid\"").append(",");
		content.append("\"token_type\"").append(":").append("\"urn:\"").append(",");
		content.append("\"expires_in\"").append(":").append(7159).append(",");
		content.append("\"client_id\"").append(":").append("\"facette\"").append("");
		content.append("}");
		
		byte[] postData			= content.toString().getBytes( StandardCharsets.UTF_8 );
		int    postDataLength	= postData.length;
		
		StringBuilder response	= new StringBuilder("HTTP/1.0 200 OK").append(HttpUtils.CRLF);
		response.append("Content-Length").append(":").append(postDataLength).append(HttpUtils.CRLF);
		response.append("Content-Type").append(":").append("application/json").append(HttpUtils.CRLF);
		response.append("HTTP-date").append(":").append(new Date()).append(HttpUtils.CRLF);
		response.append(HttpUtils.CRLF);
		response.append(content.toString());
		response.append(HttpUtils.CRLF);
		
		return response.toString();
	}

	public BufferedReader getBr() {
		return br;
	}

	public void setBr(BufferedReader br) {
		this.br = br;
	}

	public PrintWriter getPw() {
		return pw;
	}

	public void setPw(PrintWriter pw) {
		this.pw = pw;
	}

	public Thread getThread() {
		return thread;
	}

	public void setThread(Thread thread) {
		this.thread = thread;
	}

	public SSLSocket getSslClientSocket() {
		return sslClientSocket;
	}

	public void setSslClientSocket(SSLSocket sslClientSocket) {
		this.sslClientSocket = sslClientSocket;
	}
}
