package ppa.server;

import java.io.IOException;

import ppa.http.utils.Logger;

public class ServerMain {
	private static Logger<ServerMain> LOG			= Logger.getLogger(ServerMain.class);
	
	public static void main (String...args) {
		int port		= 2009;
		ServerManagerConnection serverManager	= new ServerManagerConnection(port);
		
		try {
			serverManager.initServer();
			serverManager.listenConnection();
		} catch (IOException e) {
			LOG.error("[initServer] " + "port" + " : " + serverManager.getPort());
		}
	}
}
