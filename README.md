# http-server 
needs lib-http project to work

### ssl certificate installation
##### keystore and new ciphering key pair creation
```
keytool -genkey -alias www-server-com -keyalg RSA -keysize 2048 -keystore www-server-com.jks
```

##### how to convert jks file into pkcs12 file
```
keytool -importkeystore -srckeystore [MON_KEYSTORE.jks] -destkeystore [MON_FICHIER.p12] -srcstoretype JKS -deststoretype PKCS12 -deststorepass [PASSWORD_PKCS12] -srcalias [ALIAS_SRC] -destalias [ALIAS_DEST]
```

### tomcat certificate installation

##### import certificate into keystore
```
keytool -import (-trustcacerts) -alias www-server-com -file [MON_FICHIER.crt] -keystore [nomdustockage]
```

##### tomcat ssl activation : _server.xml_
```
<Connector 
	protocol="org.apache.coyote.http11.Http11Protocol"
	port="8443" minProcessors="5" maxProcessors="75"
	enableLookups="true" disableUploadTimeout="true" 
	acceptCount="100"  maxThreads="200"
	scheme="https" secure="true" SSLEnabled="true"
	keystoreFile="CHEMIN-COMPLET+NOMDUSTOCKAGE"
	keystorePass="MOT-DE-PASSE-DU-KEYSTORE" />
```

##### de-activate SSLv2/SSLv3
```
<Connector SSLProtocol="TLSv1+TLSv1.1+TLSv1.2"/>
```

##### improve cipher quality
```
<Connector 
	ciphers="TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384,
	TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA,TLS_RSA_WITH_AES_256_GCM_SHA384,
	TLS_RSA_WITH_AES_256_CBC_SHA256,TLS_RSA_WITH_AES_256_CBC_SHA,
	TLS_RSA_WITH_CAMELLIA_256_CBC_SHA,TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
	TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA,
	TLS_RSA_WITH_AES_128_GCM_SHA256,TLS_RSA_WITH_AES_128_CBC_SHA256,
	TLS_RSA_WITH_AES_128_CBC_SHA,TLS_RSA_WITH_CAMELLIA_128_CBC_SHA" />
```